# smartcontract Voting

CZEPYGA Maxime M

Ce repo ne contient que le code du smart contract réalisé dans remix.

## Fonctionnalités ajoutées en plus des celles demandées

- Consulter toutes propositions des candidats
- En cas d'égalité pour les premiers du classement, le vot est relancé jusqu'à ce que quelqu'un soit élu avec une majorité de votes.

La vidéo de présentation est disponible [ici](./presentation.mp4)
